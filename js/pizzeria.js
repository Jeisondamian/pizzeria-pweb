var url = new URLSearchParams(location.search);
var url_api = "https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/pizzeria.json";

function getCantidadPizzas() {
    var amount = document.getElementById("amount").value;
    var form = document.getElementById("form");
    var input =
        "<div class='row border mt-2 p-4'>" +
        "<label for=''>" +
        "Tamaño de pizza @@:" +
        "<select name='size'>" +
        "<option value='Grande'>Grande</option>" +
        "<option value='Mediano'>Mediano</option>" +
        "<option value='Pequeña'>Pequeña</option>" +
        "</select>" +
        "</label></div>";

    for (let i = 0; i < amount; i++) {
        form.innerHTML += input.replace("@@", i + 1);
    }

    if (amount > 0)
        form.innerHTML += "<input type='submit'></input>";
}

function loadOptions() {
    fetch(url_api).then(response => response.json()).then(response => {
        let pizzas = response.pizzas;
        let adicional = response.adicional;
        getOptions(pizzas, adicional);
        //console.log(pizzas);
    });
}

function getOptions(pizzas, adicionales) {

    var form = document.getElementById("options");
    var sizes = url.getAll("size");

    /*  Creando un input hidden para guardar los tamaños de pizza */
    var inputHidden =
        "<input type='hidden' value='@@' name='size'>";

    /*  Creando los dropdown para escoger el sabor de la pizza*/
    var customPizza =
        //"<form action=''>" +
        "<div class='card mt-2'>" +
        "<div class='row'>" +
        "<div class='col-4'>" +
        "<img class='h-100 w-100' src='&&' alt=''>" +
        "</div>" + "<div class='col-8 card-body'>" +
        "<label for=''>" +
        "Escoja el sabor de la pizza (maximo 2 sabores):";

    customPizza = customPizza.replace("&&", pizzas[0].url_Imagen);

    var select =
        "<select name='sabor' onchange=''>";
    //+"<option value='Ninguno'>Ninguno</option>";
    var option =
        "<option value='&&'>&&</option>";

    pizzas.forEach(function(pizza) {
        select += option.replaceAll("&&", pizza.sabor);
    });
    select += "</select>";

    /*  Creando los check para escoger los adicionales */
    var customAdicionales =
        "<label for=''>" +
        "Ingredientes Adicionales:<br>";
    var check =
        "<input type='checkbox' name='adicional' value='@@'>@@</input>";
    adicionales.forEach(function(adicional) {
        customAdicionales += check.replaceAll("@@", adicional.nombre_ingrediente);
    });
    customAdicionales += "</label>";
    customPizza += select;
    customPizza += "</label><br>" + customAdicionales + "</div> </div> </div>";

    /*  Escribiendo en html */
    sizes.forEach(function(cant) {
        //customs.push(customPizza);
        form.innerHTML += customPizza + inputHidden.replace("@@", cant);
    });
    form.innerHTML += "<input type='submit'>";

}

function loadFactura() {
    fetch(url_api).then(response => response.json()).then(response => {
        getFactura(response);
    });
}

function getFactura(json) {
    var sizes = url.getAll("size");
    var sabores = url.getAll("sabor");
    var adicionales = url.getAll("adicional");
    var totalPagar = 0.1;

    var table = "";
    var thead =
        "<table class='table'>" +
        "<thead>" +
        "<tr>" +
        "<th>Descripcion</th>" +
        "<th>Valor</th>" +
        "</tr>" +
        "</thead>" +
        "<tbody>";
    var tbody =
        "<tr>" +
        "<td>@@</td>" +
        "<td>&&</td>" +
        "</tr>";

    table += thead;

    for (var i = 0; i < sizes.length; i++) {

        var pizza = json.pizzas.find(function(pizza) {
            return pizza.sabor == sabores[i];
        });

        var price = pizza.precio.find(function(p) {
            return p.tamano == sizes[i];
        });
        var pp = price.precio;
        totalPagar += pp;
        table += tbody.replace("@@", `${sabores[i]} ${sizes[i]}`).replace("&&", pp);

    }

    adicionales.forEach(function(adicional) {
        var add = json.adicional.find(function(ad) {
            return ad.nombre_ingrediente == adicional;
        });
        var price = add.valor;
        totalPagar += price;
        table += tbody.replace("@@", add.nombre_ingrediente).replace("&&", price);
    });

    table += tbody.replace("@@", " ").replace("&&", "$ " + totalPagar);
    table += "</tbody>" + "</table>";
    var factura = document.getElementById("factura");
    factura.innerHTML = table;
}